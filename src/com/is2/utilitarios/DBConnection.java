/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.is2.utilitarios;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * Clase utilitaria para realizar conexiones manuales a la base de datos.
 * Utilizada para abrir conexiones y ejecutar queries personalizados.
 * @author Gabriel Valdez, Elias Maciel, Antonella Duarte
 */
public class DBConnection {

    /**
     * Retorna una conexion a la base de datos.
     *
     * @return Connection
     * @throws Exception
     */
    public static Connection getConnection() throws Exception {

        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setUser("root");
        dataSource.setPassword("");
        dataSource.setServerName("localhost");
        dataSource.setPort(3306);
        dataSource.setDatabaseName("sistvent");
        return dataSource.getConnection();
    }

    /**
     *
     * Cierra una conexion a la base de datos.
     *
     * @param conn
     */
    public static void closeConnection(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
