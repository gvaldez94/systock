/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ClienteEditView.java
 *
 * Created on 17-abr-2016, 6:04:22
 */

package py.com.systvent.views;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import py.com.systvent.beans.Cliente;

/**
 *
 * Clase para la ventana ClienteEditView
 * @author Gabriel Valdez, Elias Maciel, Antonella Duarte
 */
public class ClienteEditView extends javax.swing.JDialog {

    /** Creates new form ClienteEditView */
    public ClienteEditView(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        label_nombre = new javax.swing.JLabel();
        label_profesion = new javax.swing.JLabel();
        label_fechanac = new javax.swing.JLabel();
        label_empresa = new javax.swing.JLabel();
        label_telempresa = new javax.swing.JLabel();
        label_ruc = new javax.swing.JLabel();
        label_rentacliente = new javax.swing.JLabel();
        label_referencia = new javax.swing.JLabel();
        label_desde = new javax.swing.JLabel();
        label_tipo = new javax.swing.JLabel();
        label_telreferencia = new javax.swing.JLabel();
        label_email = new javax.swing.JLabel();
        nombreTF = new javax.swing.JTextField();
        profesionTF = new javax.swing.JTextField();
        empresaTF = new javax.swing.JTextField();
        telempresaTF = new javax.swing.JTextField();
        rentaclienteTF = new javax.swing.JTextField();
        rucTF = new javax.swing.JTextField();
        referenciaTF = new javax.swing.JTextField();
        tipoTF = new javax.swing.JTextField();
        telreferenciaTF = new javax.swing.JTextField();
        emailTF = new javax.swing.JTextField();
        btn_confirmar = new javax.swing.JButton();
        btn_cancelar = new javax.swing.JButton();
        fechanacTF = new javax.swing.JFormattedTextField();
        fechanacTF1 = new javax.swing.JFormattedTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        label_nombre.setText("Nombre:");

        label_profesion.setText("Profesion:");

        label_fechanac.setText("Fecha Nac.");

        label_empresa.setText("Empresa:");

        label_telempresa.setText("Tel. Empresa:");

        label_ruc.setText("RUC:");

        label_rentacliente.setText("Renta Cliente:");

        label_referencia.setText("Referencia:");

        label_desde.setText("Desde:");

        label_tipo.setText("Tipo:");

        label_telreferencia.setText("Tel. Referencia:");

        label_email.setText("Email:");

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, this, org.jdesktop.beansbinding.ELProperty.create("${registroActual.nomCliente}"), nombreTF, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        nombreTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nombreTFActionPerformed(evt);
            }
        });

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, this, org.jdesktop.beansbinding.ELProperty.create("${registroActual.profesionCliente}"), profesionTF, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, this, org.jdesktop.beansbinding.ELProperty.create("${registroActual.empresaCliente}"), empresaTF, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, this, org.jdesktop.beansbinding.ELProperty.create("${registroActual.telEmpresa}"), telempresaTF, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, this, org.jdesktop.beansbinding.ELProperty.create("${registroActual.rentaCliente}"), rentaclienteTF, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, this, org.jdesktop.beansbinding.ELProperty.create("${registroActual.rucCliente}"), rucTF, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, this, org.jdesktop.beansbinding.ELProperty.create("${registroActual.refCliente}"), referenciaTF, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, this, org.jdesktop.beansbinding.ELProperty.create("${registroActual.tipoCliente}"), tipoTF, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, this, org.jdesktop.beansbinding.ELProperty.create("${registroActual.telReferencia}"), telreferenciaTF, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, this, org.jdesktop.beansbinding.ELProperty.create("${registroActual.email}"), emailTF, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        btn_confirmar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/img/accept.png"))); // NOI18N
        btn_confirmar.setText("Confirmar");
        btn_confirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_confirmarActionPerformed(evt);
                guardarNuevoCliente(evt);
            }
        });

        btn_cancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/img/remove.png"))); // NOI18N
        btn_cancelar.setText("Cancelar");
        btn_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelarNuevoCliente(evt);
            }
        });

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, this, org.jdesktop.beansbinding.ELProperty.create("${registroActual.fchNacimiento}"), fechanacTF, org.jdesktop.beansbinding.BeanProperty.create("value"));
        bindingGroup.addBinding(binding);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, this, org.jdesktop.beansbinding.ELProperty.create("${registroActual.desdeCliente}"), fechanacTF1, org.jdesktop.beansbinding.BeanProperty.create("value"));
        bindingGroup.addBinding(binding);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label_nombre)
                    .addComponent(label_email)
                    .addComponent(label_telreferencia)
                    .addComponent(label_tipo)
                    .addComponent(label_desde)
                    .addComponent(label_referencia)
                    .addComponent(label_ruc)
                    .addComponent(label_telempresa)
                    .addComponent(label_rentacliente)
                    .addComponent(label_empresa)
                    .addComponent(label_fechanac)
                    .addComponent(label_profesion))
                .addGap(53, 53, 53)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(fechanacTF1)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(btn_confirmar)
                        .addGap(79, 79, 79)
                        .addComponent(btn_cancelar))
                    .addComponent(nombreTF, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE)
                    .addComponent(profesionTF, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(empresaTF, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(telempresaTF, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rentaclienteTF, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rucTF, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(referenciaTF, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tipoTF, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(telreferenciaTF, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(emailTF, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(fechanacTF, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap(47, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_nombre)
                    .addComponent(nombreTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_profesion)
                    .addComponent(profesionTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_fechanac)
                    .addComponent(fechanacTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_empresa)
                    .addComponent(empresaTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_telempresa)
                    .addComponent(telempresaTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_rentacliente)
                    .addComponent(rentaclienteTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_ruc)
                    .addComponent(rucTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_referencia)
                    .addComponent(referenciaTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_desde)
                    .addComponent(fechanacTF1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_tipo)
                    .addComponent(tipoTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_telreferencia)
                    .addComponent(telreferenciaTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_email)
                    .addComponent(emailTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_confirmar)
                    .addComponent(btn_cancelar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nombreTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nombreTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nombreTFActionPerformed

    private void btn_confirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_confirmarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_confirmarActionPerformed

    private void guardarNuevoCliente(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guardarNuevoCliente
        // TODO add your handling code here:
        setConfirmarCliente(true);
        setVisible(false);
    }//GEN-LAST:event_guardarNuevoCliente
/**
 * 
 * @param evt 
 */
    private void cancelarNuevoCliente(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelarNuevoCliente
        // TODO add your handling code here:
        setConfirmarCliente(false);
        setVisible(false);
    }//GEN-LAST:event_cancelarNuevoCliente

   /**
    * Método main que despliega la ventana para edición de datos de Cliente.
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ClienteEditView dialog = new ClienteEditView(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    protected Cliente registroActual;

    /**
     * Get the value of registroActual
     *
     * @return the value of registroActual
     */
    public Cliente getRegistroActual() {
        return registroActual;
    }

    /**
     * Set the value of registroActual
     *
     * @param registroActual new value of registroActual
     */
    public void setRegistroActual(Cliente registroActual) {
        Cliente oldRecord = this.registroActual;
        this.registroActual = registroActual;
        propertyChangeSupport.firePropertyChange("registroActual", oldRecord, registroActual);
        
    }
    private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    /**
     * Add PropertyChangeListener.
     *
     * @param listener
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Remove PropertyChangeListener.
     *
     * @param listener
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
    protected boolean confirmarCliente;

    /**
     * Get the value of confirmarCliente
     *
     * @return the value of confirmarCliente
     */
    public boolean isConfirmarCliente() {
        return confirmarCliente;
    }

    /**
     * Set the value of confirmarCliente
     *
     * @param confirmarCliente new value of confirmarCliente
     */
    public void setConfirmarCliente(boolean confirmarCliente) {
        this.confirmarCliente = confirmarCliente;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancelar;
    private javax.swing.JButton btn_confirmar;
    private javax.swing.JTextField emailTF;
    private javax.swing.JTextField empresaTF;
    private javax.swing.JFormattedTextField fechanacTF;
    private javax.swing.JFormattedTextField fechanacTF1;
    private javax.swing.JLabel label_desde;
    private javax.swing.JLabel label_email;
    private javax.swing.JLabel label_empresa;
    private javax.swing.JLabel label_fechanac;
    private javax.swing.JLabel label_nombre;
    private javax.swing.JLabel label_profesion;
    private javax.swing.JLabel label_referencia;
    private javax.swing.JLabel label_rentacliente;
    private javax.swing.JLabel label_ruc;
    private javax.swing.JLabel label_telempresa;
    private javax.swing.JLabel label_telreferencia;
    private javax.swing.JLabel label_tipo;
    private javax.swing.JTextField nombreTF;
    private javax.swing.JTextField profesionTF;
    private javax.swing.JTextField referenciaTF;
    private javax.swing.JTextField rentaclienteTF;
    private javax.swing.JTextField rucTF;
    private javax.swing.JTextField telempresaTF;
    private javax.swing.JTextField telreferenciaTF;
    private javax.swing.JTextField tipoTF;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables

}
