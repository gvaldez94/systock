/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.systvent.beans;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *Modela el detalle del presupuesto
 * @author Gabriel Valdez, Elias Maciel, Antonella Duarte
 */
@Entity
@Table(name = "det_presupuesto", catalog = "sistvent", schema = "")
@NamedQueries({
    @NamedQuery(name = "DetPresupuesto.findAll", query = "SELECT d FROM DetPresupuesto d"),
    @NamedQuery(name = "DetPresupuesto.findByCodPresupuesto", query = "SELECT d FROM DetPresupuesto d WHERE d.detPresupuestoPK.codPresupuesto = :codPresupuesto"),
    @NamedQuery(name = "DetPresupuesto.findByCantPresuDet", query = "SELECT d FROM DetPresupuesto d WHERE d.cantPresuDet = :cantPresuDet"),
    @NamedQuery(name = "DetPresupuesto.findByVlrUnitPresuDet", query = "SELECT d FROM DetPresupuesto d WHERE d.vlrUnitPresuDet = :vlrUnitPresuDet"),
    @NamedQuery(name = "DetPresupuesto.findByVlrTotalPresuDet", query = "SELECT d FROM DetPresupuesto d WHERE d.vlrTotalPresuDet = :vlrTotalPresuDet"),
    @NamedQuery(name = "DetPresupuesto.findByCodProducto", query = "SELECT d FROM DetPresupuesto d WHERE d.detPresupuestoPK.codProducto = :codProducto")})
public class DetPresupuesto implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DetPresupuestoPK detPresupuestoPK;
    @Column(name = "cant_presu_det")
    private Integer cantPresuDet;
    @Column(name = "vlr_unit_presu_det")
    private Double vlrUnitPresuDet;
    @Column(name = "vlr_total_presu_det")
    private Double vlrTotalPresuDet;
    @JoinColumn(name = "cod_presupuesto", referencedColumnName = "cod_presupuesto", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CabPresupuesto cabPresupuesto;

    @Transient //tengo una anotación llamada @Trasient, esto informa que la propiedad no es persistente, lo que significa que la descripción del producto no será persistente
    //nueva propiedad agregada
    private String descripcionProducto;//JPA va a pedir agregar un get y un set y también va a querer persistir en una BD, pero la anotacion @Transient le indica que eso no es necesario
    /**
     * guarda descripcion del producto 
     * @param d 
     */
    public void setDescripcionProducto(String d)
    {
        this.descripcionProducto = d;
    }
/**
 * obtiene descripcion del producto
 * @return descripcionProducto
 */
    public String getDescripcionProducto()
    {
        return this.descripcionProducto;
    }

    public DetPresupuesto() {
    }

    public DetPresupuesto(DetPresupuestoPK detPresupuestoPK) {
        this.detPresupuestoPK = detPresupuestoPK;
    }

    public DetPresupuesto(int codPresupuesto, String codProducto) {
        this.detPresupuestoPK = new DetPresupuestoPK(codPresupuesto, codProducto);
    }

    public DetPresupuestoPK getDetPresupuestoPK() {
        return detPresupuestoPK;
    }
/**
 * guarda el detalle del presupuesto
 * @param detPresupuestoPK 
 */
    public void setDetPresupuestoPK(DetPresupuestoPK detPresupuestoPK) {
        this.detPresupuestoPK = detPresupuestoPK;
    }
/**
 * obtiene la cantidad del detalle del presupuesto
 * @return cantPresuDet
 */
    public Integer getCantPresuDet() {
        return cantPresuDet;
    }
/**
 * guarda la cantidad del detalle del presupuesto
 * @param cantPresuDet 
 */
    public void setCantPresuDet(Integer cantPresuDet) {
        Integer oldCantPresuDet = this.cantPresuDet;
        this.cantPresuDet = cantPresuDet;
        changeSupport.firePropertyChange("cantPresuDet", oldCantPresuDet, cantPresuDet);
    }
/**
 * obtiene el valor unitario del detalle del presupuesto
 * @return vlrUnitPresuDet
 */
    public Double getVlrUnitPresuDet() {
        return vlrUnitPresuDet;
    }
/**
 * guarda el valor unitario del detalle del presupuesto
 * @param vlrUnitPresuDet 
 */
    public void setVlrUnitPresuDet(Double vlrUnitPresuDet) {
        Double oldVlrUnitPresuDet = this.vlrUnitPresuDet;
        this.vlrUnitPresuDet = vlrUnitPresuDet;
        changeSupport.firePropertyChange("vlrUnitPresuDet", oldVlrUnitPresuDet, vlrUnitPresuDet);
    }
/**
 * obtiene el valor total del detalle del presupueto
 * @return vlrTotalPresuDet
 */
    public Double getVlrTotalPresuDet() {
        return vlrTotalPresuDet;
    }
/**
 * guarda el valor total del detalle del presupueto
 * @param vlrTotalPresuDet 
 */
    public void setVlrTotalPresuDet(Double vlrTotalPresuDet) {
        Double oldVlrTotalPresuDet = this.vlrTotalPresuDet;
        this.vlrTotalPresuDet = vlrTotalPresuDet;
        changeSupport.firePropertyChange("vlrTotalPresuDet", oldVlrTotalPresuDet, vlrTotalPresuDet);
    }
/**
 * obtiene la cabecera del presupuesto
 * @return cabPresupuesto
 */
    public CabPresupuesto getCabPresupuesto() {
        return cabPresupuesto;
    }
/**
 * guarda la cabecera del presupuesto
 * @param cabPresupuesto 
 */
    public void setCabPresupuesto(CabPresupuesto cabPresupuesto) {
        CabPresupuesto oldCabPresupuesto = this.cabPresupuesto;
        this.cabPresupuesto = cabPresupuesto;
        changeSupport.firePropertyChange("cabPresupuesto", oldCabPresupuesto, cabPresupuesto);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detPresupuestoPK != null ? detPresupuestoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetPresupuesto)) {
            return false;
        }
        DetPresupuesto other = (DetPresupuesto) object;
        if ((this.detPresupuestoPK == null && other.detPresupuestoPK != null) || (this.detPresupuestoPK != null && !this.detPresupuestoPK.equals(other.detPresupuestoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.systvent.beans.DetPresupuesto[detPresupuestoPK=" + detPresupuestoPK + "]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

}
