/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.systvent.beans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *Modela el detalle del presupuesto
 * @author Gabriel Valdez, Elias Maciel, Antonella Duarte
 */
@Embeddable
public class DetPresupuestoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "cod_presupuesto")
    private int codPresupuesto;
    @Basic(optional = false)
    @Column(name = "cod_producto")
    private String codProducto;

    public DetPresupuestoPK() {
    }

    public DetPresupuestoPK(int codPresupuesto, String codProducto) {
        this.codPresupuesto = codPresupuesto;
        this.codProducto = codProducto;
    }
/**
 * obtiene el codigo del presupuesto
 * @return 
 */
    public int getCodPresupuesto() {
        return codPresupuesto;
    }
/**
 * guarda el codigo del presupuesto
 * @param codPresupuesto 
 */
    public void setCodPresupuesto(int codPresupuesto) {
        this.codPresupuesto = codPresupuesto;
    }
/**
 * obtiene el codigo del producto
 * @return codProducto
 */
    public String getCodProducto() {
        return codProducto;
    }
/**
 * guarda el codigo del producto
 * @param codProducto 
 */
    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += codPresupuesto;
        hash += (codProducto != null ? codProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetPresupuestoPK)) {
            return false;
        }
        DetPresupuestoPK other = (DetPresupuestoPK) object;
        if (this.codPresupuesto != other.codPresupuesto) {
            return false;
        }
        if ((this.codProducto == null && other.codProducto != null) || (this.codProducto != null && !this.codProducto.equals(other.codProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.systvent.beans.DetPresupuestoPK[codPresupuesto=" + codPresupuesto + ", codProducto=" + codProducto + "]";
    }

}
