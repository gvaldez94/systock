/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.systvent.beans;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *Clase que modela la Cabecera del Presupuesto
 * @author Gabriel Valdez, Elias Maciel, Antonella Duarte
 */
@Entity
@Table(name = "cab_presupuesto", catalog = "sistvent", schema = "")
@NamedQueries({
    @NamedQuery(name = "CabPresupuesto.findAll", query = "SELECT c FROM CabPresupuesto c"),
    @NamedQuery(name = "CabPresupuesto.findByCodPresupuesto", query = "SELECT c FROM CabPresupuesto c WHERE c.codPresupuesto = :codPresupuesto"),
    @NamedQuery(name = "CabPresupuesto.findByFchPresCab", query = "SELECT c FROM CabPresupuesto c WHERE c.fchPresCab = :fchPresCab"),
    @NamedQuery(name = "CabPresupuesto.findByVlrPresuCab", query = "SELECT c FROM CabPresupuesto c WHERE c.vlrPresuCab = :vlrPresuCab"),
    @NamedQuery(name = "CabPresupuesto.findByCodCliente", query = "SELECT c FROM CabPresupuesto c WHERE c.codCliente = :codCliente"),
    @NamedQuery(name = "CabPresupuesto.findByCodFuncionario", query = "SELECT c FROM CabPresupuesto c WHERE c.codFuncionario = :codFuncionario")})


public class CabPresupuesto implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_presupuesto")
    private Integer codPresupuesto;
    @Column(name = "fch_pres_cab")
    @Temporal(TemporalType.DATE)
    private Date fchPresCab;
    @Column(name = "vlr_presu_cab")
    private Double vlrPresuCab;
    @Column(name = "cod_cliente")
    private Integer codCliente;
    @Column(name = "cod_funcionario")
    private Integer codFuncionario;
/*
    @JoinColumn(name = "cod_funcionario", referencedColumnName = "cod_funcionario")
    @ManyToOne
    
    private Integer codFuncionario;
*/
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cabPresupuesto")
    private List<DetPresupuesto> detPresupuestoList;

    public CabPresupuesto() {
    }

    public CabPresupuesto(Integer codPresupuesto) {
        this.codPresupuesto = codPresupuesto;
    }

    /**
     * Obtiene el codigo del presupuesto
     * @return codPrpresupuesto
     */
    public Integer getCodPresupuesto() {
        return codPresupuesto;
    }
/**
 * Guarda el codPresupuesto
 * @param codPresupuesto 
 */
    public void setCodPresupuesto(Integer codPresupuesto) {
        Integer oldCodPresupuesto = this.codPresupuesto;
        this.codPresupuesto = codPresupuesto;
        changeSupport.firePropertyChange("codPresupuesto", oldCodPresupuesto, codPresupuesto);
    }
/**
 * Obtiene la fecha de la cabecera del presupuesto
 * @return FchPresCab
 */
    public Date getFchPresCab() {
        return fchPresCab;
    }
/**
 * Guarda la fecha de la cabecera del presupuesto
 * @param fchPresCab 
 */
    public void setFchPresCab(Date fchPresCab) {
        Date oldFchPresCab = this.fchPresCab;
        this.fchPresCab = fchPresCab;
        changeSupport.firePropertyChange("fchPresCab", oldFchPresCab, fchPresCab);
    }
/**
 * 
 * @return valor de la cabecera del presupuesto
 */
    public Double getVlrPresuCab() {
        return vlrPresuCab;
    }
/**
 * Guarda valor de la cabecera del presupuesto
 * @param vlrPresuCab 
 */
    public void setVlrPresuCab(Double vlrPresuCab) {
        Double oldVlrPresuCab = this.vlrPresuCab;
        this.vlrPresuCab = vlrPresuCab;
        changeSupport.firePropertyChange("vlrPresuCab", oldVlrPresuCab, vlrPresuCab);
    }
/**
 * Obtiene el codigo del cliente
 * @return codcliente
 */
    public Integer getCodCliente() {
        return codCliente;
    }
/**
 * Guarda el codigo del cliente
 * @param codCliente 
 */
    public void setCodCliente(Integer codCliente) {
        Integer oldCodCliente = this.codCliente;
        this.codCliente = codCliente;
        changeSupport.firePropertyChange("codCliente", oldCodCliente, codCliente);
    }
/**
 * Obtiene codigo del Funcionario
 * @return codFuncionario
 */
    public Integer getCodFuncionario() {
        return codFuncionario;
    }
/**
 * Guarda el cofigo del funcionario
 * @param codFuncionario 
 */
    public void setCodFuncionario(Integer codFuncionario) {
        Integer oldCodFuncionario = this.codFuncionario;
        this.codFuncionario = codFuncionario;
        changeSupport.firePropertyChange("codFuncionario", oldCodFuncionario, codFuncionario);
    }
/**
 * Retorna una lista del detalle del Presupuesto
 * @return detPresupuestoList
 */
    public List<DetPresupuesto> getDetPresupuestoList() {
        return detPresupuestoList;
    }
/**
 * Guarda la lista del detalle del presupuesto
 * @param detPresupuestoList 
 */
    public void setDetPresupuestoList(List<DetPresupuesto> detPresupuestoList) {
        this.detPresupuestoList = detPresupuestoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codPresupuesto != null ? codPresupuesto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CabPresupuesto)) {
            return false;
        }
        CabPresupuesto other = (CabPresupuesto) object;
        if ((this.codPresupuesto == null && other.codPresupuesto != null) || (this.codPresupuesto != null && !this.codPresupuesto.equals(other.codPresupuesto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.systvent.beans.CabPresupuesto[codPresupuesto=" + codPresupuesto + "]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

}
