/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.systvent.beans;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Mdela la Cabecera la Venta (venta total)
 * @author Gabriel Valdez -- Elias Maciel -- Antonella Duarte
 */
@Entity
@Table(name = "venta_cabecera", catalog = "sistvent", schema = "")
@NamedQueries({
    @NamedQuery(name = "VentaCabecera.findAll", query = "SELECT v FROM VentaCabecera v"),
    @NamedQuery(name = "VentaCabecera.findByCodVentaCab", query = "SELECT v FROM VentaCabecera v WHERE v.codVentaCab = :codVentaCab"),
    @NamedQuery(name = "VentaCabecera.findByCodTarjeta", query = "SELECT v FROM VentaCabecera v WHERE v.codTarjeta = :codTarjeta"),
    @NamedQuery(name = "VentaCabecera.findByCodTipoPago", query = "SELECT v FROM VentaCabecera v WHERE v.codTipoPago = :codTipoPago"),
    @NamedQuery(name = "VentaCabecera.findByCodCliente", query = "SELECT v FROM VentaCabecera v WHERE v.codCliente = :codCliente"),
    @NamedQuery(name = "VentaCabecera.findByFchVentaCab", query = "SELECT v FROM VentaCabecera v WHERE v.fchVentaCab = :fchVentaCab"),
    @NamedQuery(name = "VentaCabecera.findByValorVentaCab", query = "SELECT v FROM VentaCabecera v WHERE v.valorVentaCab = :valorVentaCab"),
    @NamedQuery(name = "VentaCabecera.findByDescuentoVentaCab", query = "SELECT v FROM VentaCabecera v WHERE v.descuentoVentaCab = :descuentoVentaCab"),
    @NamedQuery(name = "VentaCabecera.findByTotalVentaCab", query = "SELECT v FROM VentaCabecera v WHERE v.totalVentaCab = :totalVentaCab"),
    @NamedQuery(name = "VentaCabecera.findByPriVencVentaCab", query = "SELECT v FROM VentaCabecera v WHERE v.priVencVentaCab = :priVencVentaCab"),
    @NamedQuery(name = "VentaCabecera.findByNumReciboVentaCab", query = "SELECT v FROM VentaCabecera v WHERE v.numReciboVentaCab = :numReciboVentaCab"),
    @NamedQuery(name = "VentaCabecera.findByCodFuncionario", query = "SELECT v FROM VentaCabecera v WHERE v.codFuncionario = :codFuncionario")})
public class VentaCabecera implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "cod_venta_cab")
    private Integer codVentaCab;
    //@Column(name = "cod_tarjeta")
    //private Integer codTarjeta;
    @JoinColumn(name = "cod_tarjeta", referencedColumnName = "cod_tarjeta")
    @ManyToOne
    private Tarjeta codTarjeta;
    //@Column(name = "cod_tipo_pago")
    //private Integer codTipoPago;
    @JoinColumn(name = "cod_tipo_pago", referencedColumnName = "cod_tipo_pago")
    @ManyToOne
    private TipoPago codTipoPago;
    @Column(name = "cod_cliente")
    private Integer codCliente;
    @Column(name = "fch_venta_cab")
    @Temporal(TemporalType.DATE)
    private Date fchVentaCab;
    @Basic(optional = false)
    @Column(name = "valor_venta_cab")
    private double valorVentaCab;
    @Column(name = "descuento_venta_cab")
    private Double descuentoVentaCab;
    @Column(name = "total_venta_cab")
    private Double totalVentaCab;
    @Column(name = "pri_venc_venta_cab")
    @Temporal(TemporalType.DATE)
    private Date priVencVentaCab;
    @Column(name = "num_recibo_venta_cab")
    private Integer numReciboVentaCab;
    @Column(name = "cod_funcionario")
    private Integer codFuncionario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ventaCabecera")
    private List<VentaDet> ventaDetList;

    public VentaCabecera() {
    }

    public VentaCabecera(Integer codVentaCab) {
        this.codVentaCab = codVentaCab;
    }

    public VentaCabecera(Integer codVentaCab, double valorVentaCab) {
        this.codVentaCab = codVentaCab;
        this.valorVentaCab = valorVentaCab;
    }
     /**
      * Obtiene el codigo de la Cabecera de la Venta
      * @return codVentaCab
      */
    public Integer getCodVentaCab() {
        return codVentaCab;
    }

    /**
     * Guarda el codigo de la Cabecera de la Venta
     * @param codVentaCab
     */
    public void setCodVentaCab(Integer codVentaCab) {
        Integer oldCodVentaCab = this.codVentaCab;
        this.codVentaCab = codVentaCab;
        changeSupport.firePropertyChange("codVentaCab", oldCodVentaCab, codVentaCab);
    }

    /**
      * Obtiene el codigo de la Tarjeta
      * @return codTarjeta
      */
    public Tarjeta getCodTarjeta() {
        return codTarjeta;
    }

    /**
     * Guarda el codigo de la Tarjeta
     * @param codTarjeta
     */
    public void setCodTarjeta(Tarjeta codTarjeta) {
        Tarjeta oldCodTarjeta = this.codTarjeta;
        this.codTarjeta = codTarjeta;
        changeSupport.firePropertyChange("codTarjeta", oldCodTarjeta, codTarjeta);
    }

    /**
      * Obtiene el codigo del Tipo de Pago
      * @return codTipoPago
      */
    public TipoPago getCodTipoPago() {
        return codTipoPago;
    }

    /**
     * Guarda el codigo del Tipo de pago
     * @param codTipoPago
     */
    public void setCodTipoPago(TipoPago codTipoPago) {
        TipoPago oldCodTipoPago = this.codTipoPago;
        this.codTipoPago = codTipoPago;
        changeSupport.firePropertyChange("codTipoPago", oldCodTipoPago, codTipoPago);
    }

     /**
      * Obtiene el codigo del Cliente
      * @return codCliente
      */
    public Integer getCodCliente() {
        return codCliente;
    }

    /**
     * Guarda el codigo del Cliente
     * @param codCliente
     */
    public void setCodCliente(Integer codCliente) {
        Integer oldCodCliente = this.codCliente;
        this.codCliente = codCliente;
        changeSupport.firePropertyChange("codCliente", oldCodCliente, codCliente);
    }

    /**
     * Obtiene la fecha de la Venta
     * @return fchVentaCab
     */
    public Date getFchVentaCab() {
        return fchVentaCab;
    }

    /**
     * Guarda la fecha de la Venta.
     * @param fchVentaCab
     */
    public void setFchVentaCab(Date fchVentaCab) {
        Date oldFchVentaCab = this.fchVentaCab;
        this.fchVentaCab = fchVentaCab;
        changeSupport.firePropertyChange("fchVentaCab", oldFchVentaCab, fchVentaCab);
    }

    /**
     * Obtiene el valor de la Venta
     * @return valorVentaCab
     */
    public double getValorVentaCab() {
        return valorVentaCab;
    }

    /**
     * Guarda el valor de la Venta.
     * @param valorVentaCab
     */
    public void setValorVentaCab(double valorVentaCab) {
        double oldValorVentaCab = this.valorVentaCab;
        this.valorVentaCab = valorVentaCab;
        changeSupport.firePropertyChange("valorVentaCab", oldValorVentaCab, valorVentaCab);
    }

    /**
      * Obtiene el descuento de la Cabecera de la venta
      * @return descuentoVentaCab
      */
    public Double getDescuentoVentaCab() {
        return descuentoVentaCab;
    }

    /**
     * Guarda el descuento de la Cabecera de la Venta.
     * @param descuentoVentaCab
     */
    public void setDescuentoVentaCab(Double descuentoVentaCab) {
        Double oldDescuentoVentaCab = this.descuentoVentaCab;
        this.descuentoVentaCab = descuentoVentaCab;
        changeSupport.firePropertyChange("descuentoVentaCab", oldDescuentoVentaCab, descuentoVentaCab);
    }

    /**
      * Obtiene el total de la venta
      * @return totalVentaCab
      */
    public Double getTotalVentaCab() {
        return totalVentaCab;
    }

    /**
     * Guarda el total de la Cabecera de la Venta.
     * @param totalVentaCab
     */
    public void setTotalVentaCab(Double totalVentaCab) {
        Double oldTotalVentaCab = this.totalVentaCab;
        this.totalVentaCab = totalVentaCab;
        changeSupport.firePropertyChange("totalVentaCab", oldTotalVentaCab, totalVentaCab);
    }

    /**
     * Obtiene el primer vencimiento de la Cabecera de la Venta
     * @return priVencVentaCab
     */
    public Date getPriVencVentaCab() {
        return priVencVentaCab;
    }

    public void setPriVencVentaCab(Date priVencVentaCab) {
        Date oldPriVencVentaCab = this.priVencVentaCab;
        this.priVencVentaCab = priVencVentaCab;
        changeSupport.firePropertyChange("priVencVentaCab", oldPriVencVentaCab, priVencVentaCab);
    }

    /**
     * Obtiene el numero del recibo
     * @return numReciboVentaCab
     */
    public Integer getNumReciboVentaCab() {
        return numReciboVentaCab;
    }

    /**
     * Guarda el numReciboVentaCab
     * @param numReciboVentaCab
     */
    public void setNumReciboVentaCab(Integer numReciboVentaCab) {
        Integer oldNumReciboVentaCab = this.numReciboVentaCab;
        this.numReciboVentaCab = numReciboVentaCab;
        changeSupport.firePropertyChange("numReciboVentaCab", oldNumReciboVentaCab, numReciboVentaCab);
    }

    /**
     * Obtiene el codigo del funcionario
     * @return codFuncionario
     */
    public Integer getCodFuncionario() {
        return codFuncionario;
    }

    /**
     * Guarda el codigo del Funcionario
     * @param codFuncionario
     */
    public void setCodFuncionario(Integer codFuncionario) {
        Integer oldCodFuncionario = this.codFuncionario;
        this.codFuncionario = codFuncionario;
        changeSupport.firePropertyChange("codFuncionario", oldCodFuncionario, codFuncionario);
    }

    /**
     * Obtiene el detalle de la Venta en una lista
     * @return ventaDetList
     */
    public List<VentaDet> getVentaDetList() {
        return ventaDetList;
    }

    /**
     * Guarda el codigo del Funcionario
     * @param ventaDetList
     */
    public void setVentaDetList(List<VentaDet> ventaDetList) {
        this.ventaDetList = ventaDetList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codVentaCab != null ? codVentaCab.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VentaCabecera)) {
            return false;
        }
        VentaCabecera other = (VentaCabecera) object;
        if ((this.codVentaCab == null && other.codVentaCab != null) || (this.codVentaCab != null && !this.codVentaCab.equals(other.codVentaCab))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.systvent.views.VentaCabecera[codVentaCab=" + codVentaCab + "]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

}
