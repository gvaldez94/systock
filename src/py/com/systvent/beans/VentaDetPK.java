/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.systvent.beans;

import com.is2.utilitarios.DBConnection;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Modela los codigos de la Cabecera y el Produto para manejarlos como clave compuesta
 * @author Gabriel Valdez -- Elias Maciel -- Antonella Duarte
 */
@Embeddable
public class VentaDetPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "cod_venta_cab")
    private int codVentaCab;
    @Basic(optional = false)
    @Column(name = "cod_producto")
    private String codProducto;

    public VentaDetPK() {
    }

    public VentaDetPK(int codVentaCab, String codProducto) {
        this.codVentaCab = codVentaCab;
        this.codProducto = codProducto;
    }

    /**
      * Obtiene el codigo de la Cabecera de la Venta
      * @return codVentaCab
      */
    public int getCodVentaCab() {
        return codVentaCab;
    }

    /**
     * Guarda el codigo de la Cabecera de la Venta
     * @param codVentaCab
     */
    public void setCodVentaCab(int codVentaCab) {
        this.codVentaCab = codVentaCab;
    }

    /**
      * Obtiene el codigo del producto
      * @return codProducto
      */
    public String getCodProducto() {
        return codProducto;
    }

    /**
     * Guarda el codigo del producto
     * @param codProducto
     */
    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) codVentaCab;
        hash += (codProducto != null ? codProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VentaDetPK)) {
            return false;
        }
        VentaDetPK other = (VentaDetPK) object;
        if (this.codVentaCab != other.codVentaCab) {
            return false;
        }
        if ((this.codProducto == null && other.codProducto != null) || (this.codProducto != null && !this.codProducto.equals(other.codProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        //return "py.com.systvent.views.VentaDetPK[codVentaCab=" + codVentaCab + ", codProducto=" + codProducto + "]";
        String descProducto = "";
        try {
            DBConnection dbconn = new DBConnection();
            Connection conn = dbconn.getConnection();
            String sql = "SELECT desc_producto FROM producto WHERE cod_producto = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, this.codProducto);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            descProducto = rs.getString(1);
            rs.close();
            stmt.close();
            conn.close();

        } catch(Exception e) {
            e.printStackTrace();
        }
        return descProducto;
    }

}
