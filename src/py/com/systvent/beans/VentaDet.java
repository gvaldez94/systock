/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.systvent.beans;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Modela el Detalle de la Venta.
 * @author Gabriel Vazdez -- Elias Maciel -- Antonella Duarte
 */
@Entity
@Table(name = "venta_det", catalog = "sistvent", schema = "")
@NamedQueries({
    @NamedQuery(name = "VentaDet.findAll", query = "SELECT v FROM VentaDet v"),
    @NamedQuery(name = "VentaDet.findByCodVentaCab", query = "SELECT v FROM VentaDet v WHERE v.ventaDetPK.codVentaCab = :codVentaCab"),
    @NamedQuery(name = "VentaDet.findByCodProducto", query = "SELECT v FROM VentaDet v WHERE v.ventaDetPK.codProducto = :codProducto"),
    @NamedQuery(name = "VentaDet.findByCantVentaDet", query = "SELECT v FROM VentaDet v WHERE v.cantVentaDet = :cantVentaDet"),
    @NamedQuery(name = "VentaDet.findByVlrUnitVentaDet", query = "SELECT v FROM VentaDet v WHERE v.vlrUnitVentaDet = :vlrUnitVentaDet"),
    @NamedQuery(name = "VentaDet.findByVlrTotalVentaDet", query = "SELECT v FROM VentaDet v WHERE v.vlrTotalVentaDet = :vlrTotalVentaDet")})
public class VentaDet implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected VentaDetPK ventaDetPK;
    @Column(name = "cant_venta_det")
    private Integer cantVentaDet;
    @Column(name = "vlr_unit_venta_det")
    private Double vlrUnitVentaDet;
    @Column(name = "vlr_total_venta_det")
    private Double vlrTotalVentaDet;
    @JoinColumn(name = "cod_venta_cab", referencedColumnName = "cod_venta_cab", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private VentaCabecera ventaCabecera;

    public VentaDet() {
    }

    public VentaDet(VentaDetPK ventaDetPK) {
        this.ventaDetPK = ventaDetPK;
    }


    public VentaDet(int codVentaCab, String codProducto) {
        this.ventaDetPK = new VentaDetPK(codVentaCab, codProducto);
    }

    /**
     * Obtiene el pk del Detalle de la Venta
     * @return ventaDetPK
     */
    public VentaDetPK getVentaDetPK() {
        return ventaDetPK;
    }

    /**
     * Guarda el codigo del Detalle de la Venta
     * @param ventaDetPK
     */
    public void setVentaDetPK(VentaDetPK ventaDetPK) {
        this.ventaDetPK = ventaDetPK;
    }

    /**
     * Obtiene la cantidad vendida de un determinado producto
     * @return cantVentaDet
     */
    public Integer getCantVentaDet() {
        return cantVentaDet;
    }

    /**
     * Guarda la cantidad del Detalle de la Venta
     * @param cantVentaDet
     */
    public void setCantVentaDet(Integer cantVentaDet) {
        Integer oldCantVentaDet = this.cantVentaDet;
        this.cantVentaDet = cantVentaDet;
        changeSupport.firePropertyChange("cantVentaDet", oldCantVentaDet, cantVentaDet);
    }

    /**
     * Obtiene el valor unitario
     * @return vlrUnitVentaDet
     */
    public Double getVlrUnitVentaDet() {
        return vlrUnitVentaDet;
    }

    /**
     * Guarda el valor unitario del Detalle de la Venta
     * @param vlrUnitVentaDet
     */
    public void setVlrUnitVentaDet(Double vlrUnitVentaDet) {
        Double oldVlrUnitVentaDet = this.vlrUnitVentaDet;
        this.vlrUnitVentaDet = vlrUnitVentaDet;
        changeSupport.firePropertyChange("vlrUnitVentaDet", oldVlrUnitVentaDet, vlrUnitVentaDet);
    }
    /**
     * Obtiene el valor total del detalle de la Venta
     * @return vlrTotalVentaDet
     */
    public Double getVlrTotalVentaDet() {
        return vlrTotalVentaDet;
    }

    /**
     * Guarda el valor total del Detalle de la Venta
     * @param vlrTotalVentaDet
     */
    public void setVlrTotalVentaDet(Double vlrTotalVentaDet) {
        Double oldVlrTotalVentaDet = this.vlrTotalVentaDet;
        this.vlrTotalVentaDet = vlrTotalVentaDet;
        changeSupport.firePropertyChange("vlrTotalVentaDet", oldVlrTotalVentaDet, vlrTotalVentaDet);
    }

    /**
     * Obtiene el objeto VentaCabecera
     * @return ventaCabecera
     */
    public VentaCabecera getVentaCabecera() {
        return ventaCabecera;
    }

    /**
     * Guarda el  objeto VentaCabecera
     * @param ventaCabecera
     */
    public void setVentaCabecera(VentaCabecera ventaCabecera) {
        VentaCabecera oldVentaCabecera = this.ventaCabecera;
        this.ventaCabecera = ventaCabecera;
        changeSupport.firePropertyChange("ventaCabecera", oldVentaCabecera, ventaCabecera);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ventaDetPK != null ? ventaDetPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VentaDet)) {
            return false;
        }
        VentaDet other = (VentaDet) object;
        if ((this.ventaDetPK == null && other.ventaDetPK != null) || (this.ventaDetPK != null && !this.ventaDetPK.equals(other.ventaDetPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.systvent.views.VentaDet[ventaDetPK=" + ventaDetPK + "]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

}
