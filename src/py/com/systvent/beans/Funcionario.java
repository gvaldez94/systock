/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.systvent.beans;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *Modela al Funcionario
 * @author Gabriel Valdez, Elias Maciel, Antonella Duarte
 */
@Entity
@Table(name = "funcionario", catalog = "sistvent", schema = "")
@NamedQueries({
    @NamedQuery(name = "Funcionario.findAll", query = "SELECT f FROM Funcionario f"),
    @NamedQuery(name = "Funcionario.findByCodFuncionario", query = "SELECT f FROM Funcionario f WHERE f.codFuncionario = :codFuncionario"),
    @NamedQuery(name = "Funcionario.findByNombreFuncionario", query = "SELECT f FROM Funcionario f WHERE f.nombreFuncionario = :nombreFuncionario"),
    @NamedQuery(name = "Funcionario.findByFuncionFuncionario", query = "SELECT f FROM Funcionario f WHERE f.funcionFuncionario = :funcionFuncionario"),
    @NamedQuery(name = "Funcionario.findByLoginFuncionario", query = "SELECT f FROM Funcionario f WHERE f.loginFuncionario = :loginFuncionario"),
    @NamedQuery(name = "Funcionario.findBySenhaFuncionario", query = "SELECT f FROM Funcionario f WHERE f.senhaFuncionario = :senhaFuncionario"),
    @NamedQuery(name = "Funcionario.findByAdmisionFuncionario", query = "SELECT f FROM Funcionario f WHERE f.admisionFuncionario = :admisionFuncionario"),
    @NamedQuery(name = "Funcionario.findByAccesoTotal", query = "SELECT f FROM Funcionario f WHERE f.accesoTotal = :accesoTotal"),
    @NamedQuery(name = "Funcionario.findByCodDepartamento", query = "SELECT f FROM Funcionario f WHERE f.codDepartamento = :codDepartamento")})
public class Funcionario implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_funcionario")
    private Integer codFuncionario;
    @Column(name = "nombre_funcionario")
    private String nombreFuncionario;
    @Column(name = "funcion_funcionario")
    private String funcionFuncionario;
    @Column(name = "login_funcionario")
    private String loginFuncionario;
    @Column(name = "senha_funcionario")
    private String senhaFuncionario;
    @Column(name = "admision_funcionario")
    @Temporal(TemporalType.DATE)
    private Date admisionFuncionario;
    @Column(name = "acceso_total")
    private Character accesoTotal;
    @Column(name = "cod_departamento")
    private Integer codDepartamento;

    public Funcionario() {
    }

    public Funcionario(Integer codFuncionario) {
        this.codFuncionario = codFuncionario;
    }
    /**
     * obtiene el codigo del funcionario
     * @return codFuncionario
     */

    public Integer getCodFuncionario() {
        return codFuncionario;
    }
/**
 * guarda el codigo del funcionario
 * @param codFuncionario 
 */
    public void setCodFuncionario(Integer codFuncionario) {
        Integer oldCodFuncionario = this.codFuncionario;
        this.codFuncionario = codFuncionario;
        changeSupport.firePropertyChange("codFuncionario", oldCodFuncionario, codFuncionario);
    }
/**
 * obtiene el nombre del funcionario
 * @return nombreFuncionario
 */
    public String getNombreFuncionario() {
        return nombreFuncionario;
    }
/**
 * guarda el nombre del funcionario
 * @param nombreFuncionario 
 */
    public void setNombreFuncionario(String nombreFuncionario) {
        String oldNombreFuncionario = this.nombreFuncionario;
        this.nombreFuncionario = nombreFuncionario;
        changeSupport.firePropertyChange("nombreFuncionario", oldNombreFuncionario, nombreFuncionario);
    }
/**
 * obtiene la funcion del funcionario
 * @return funcionFuncionario
 */
    public String getFuncionFuncionario() {
        return funcionFuncionario;
    }
/**
 * guarda la funcion del funcionario
 * @param funcionFuncionario 
 */
    public void setFuncionFuncionario(String funcionFuncionario) {
        String oldFuncionFuncionario = this.funcionFuncionario;
        this.funcionFuncionario = funcionFuncionario;
        changeSupport.firePropertyChange("funcionFuncionario", oldFuncionFuncionario, funcionFuncionario);
    }
/**
 * obtiene el login del funcionario
 * @return loginFuncionario
 */
    public String getLoginFuncionario() {
        return loginFuncionario;
    }
/**
 * guarda el login del funcionario
 * @param loginFuncionario 
 */
    public void setLoginFuncionario(String loginFuncionario) {
        String oldLoginFuncionario = this.loginFuncionario;
        this.loginFuncionario = loginFuncionario;
        changeSupport.firePropertyChange("loginFuncionario", oldLoginFuncionario, loginFuncionario);
    }
/**
 * obtiene la seña del funcionario
 * @return senhaFuncionario
 */
    public String getSenhaFuncionario() {
        return senhaFuncionario;
    }
/**
 * guarda la seña del funcionario
 * @param senhaFuncionario 
 */
    public void setSenhaFuncionario(String senhaFuncionario) {
        String oldSenhaFuncionario = this.senhaFuncionario;
        this.senhaFuncionario = senhaFuncionario;
        changeSupport.firePropertyChange("senhaFuncionario", oldSenhaFuncionario, senhaFuncionario);
    }
/**
 * obtiene la admision del funcionario
 * @return admisionFuncionario
 */
    public Date getAdmisionFuncionario() {
        return admisionFuncionario;
    }
/**
 * guarda la admision del funcionario
 * @param admisionFuncionario 
 */
    public void setAdmisionFuncionario(Date admisionFuncionario) {
        Date oldAdmisionFuncionario = this.admisionFuncionario;
        this.admisionFuncionario = admisionFuncionario;
        changeSupport.firePropertyChange("admisionFuncionario", oldAdmisionFuncionario, admisionFuncionario);
    }
/**
 * obtiene el acceso total
 * @return accesoTotal
 */
    public Character getAccesoTotal() {
        return accesoTotal;
    }
/**
 * guarda el acceso total
 * @param accesoTotal 
 */
    public void setAccesoTotal(Character accesoTotal) {
        Character oldAccesoTotal = this.accesoTotal;
        this.accesoTotal = accesoTotal;
        changeSupport.firePropertyChange("accesoTotal", oldAccesoTotal, accesoTotal);
    }
/**
 * obtiene el codigo del departamento
 * @return codDepartamento
 */
    public Integer getCodDepartamento() {
        return codDepartamento;
    }
/**
 * guarda el codigo del departamento
 * @param codDepartamento 
 */
    public void setCodDepartamento(Integer codDepartamento) {
        Integer oldCodDepartamento = this.codDepartamento;
        this.codDepartamento = codDepartamento;
        changeSupport.firePropertyChange("codDepartamento", oldCodDepartamento, codDepartamento);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codFuncionario != null ? codFuncionario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Funcionario)) {
            return false;
        }
        Funcionario other = (Funcionario) object;
        if ((this.codFuncionario == null && other.codFuncionario != null) || (this.codFuncionario != null && !this.codFuncionario.equals(other.codFuncionario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.systvent.bean.Funcionario[codFuncionario=" + codFuncionario + "]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

}
