/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.systvent.beans;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *Modela al producto
 * @author Maciel,  Valdez, Duarte
 */
@Entity
@Table(name = "producto", catalog = "sistvent", schema = "")
@NamedQueries({
    @NamedQuery(name = "Producto.findAll", query = "SELECT p FROM Producto p"),
    @NamedQuery(name = "Producto.findByCodProducto", query = "SELECT p FROM Producto p WHERE p.codProducto = :codProducto"),
    @NamedQuery(name = "Producto.findByDescProducto", query = "SELECT p FROM Producto p WHERE p.descProducto = :descProducto"),
    @NamedQuery(name = "Producto.findByPrecioCompraProducto", query = "SELECT p FROM Producto p WHERE p.precioCompraProducto = :precioCompraProducto"),
    @NamedQuery(name = "Producto.findByPrecioVentaProducto", query = "SELECT p FROM Producto p WHERE p.precioVentaProducto = :precioVentaProducto"),
    @NamedQuery(name = "Producto.findByCntStockProducto", query = "SELECT p FROM Producto p WHERE p.cntStockProducto = :cntStockProducto"),
    @NamedQuery(name = "Producto.findByCntStockCriticoProducto", query = "SELECT p FROM Producto p WHERE p.cntStockCriticoProducto = :cntStockCriticoProducto"),
    @NamedQuery(name = "Producto.findByCodUnidad", query = "SELECT p FROM Producto p WHERE p.codUnidad = :codUnidad"),
    @NamedQuery(name = "Producto.findByCodProveedor", query = "SELECT p FROM Producto p WHERE p.codProveedor = :codProveedor")})
public class Producto implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "cod_producto")
    private String codProducto;
    @Column(name = "desc_producto")
    private String descProducto;
    @Column(name = "precio_compra_producto")
    private Double precioCompraProducto;
    @Column(name = "precio_venta_producto")
    private Double precioVentaProducto;
    @Column(name = "cnt_stock_producto")
    private Integer cntStockProducto;
    @Column(name = "cnt_stock_critico_producto")
    private Integer cntStockCriticoProducto;
    @Column(name = "cod_unidad")
    private Integer codUnidad;
    @Column(name = "cod_proveedor")
    private Integer codProveedor;

    public Producto() {
    }

    public Producto(String codProducto) {
        this.codProducto = codProducto;
    }
/**
 * obtiene en un string el codigo producto
 * @return codProducto
 */
    public String getCodProducto() {
        return codProducto;
    }
/**
 * guarda el codigo del producto
 * @param codProducto 
 */
    public void setCodProducto(String codProducto) {
        String oldCodProducto = this.codProducto;
        this.codProducto = codProducto;
        changeSupport.firePropertyChange("codProducto", oldCodProducto, codProducto);
    }
/**
 * obtiene la descripcion del producto
 * @return descProducto
 */
    public String getDescProducto() {
        return descProducto;
    }
/**
 * guarda la descripcion del producto
 * @param descProducto 
 */
    public void setDescProducto(String descProducto) {
        String oldDescProducto = this.descProducto;
        this.descProducto = descProducto;
        changeSupport.firePropertyChange("descProducto", oldDescProducto, descProducto);
    }
/**
 * obtiene el precio de compra del producto
 * @return precioCompraProducto
 */
    public Double getPrecioCompraProducto() {
        return precioCompraProducto;
    }
/**
 * guarda el precio de compra del producto
 * @param precioCompraProducto 
 */
    public void setPrecioCompraProducto(Double precioCompraProducto) {
        Double oldPrecioCompraProducto = this.precioCompraProducto;
        this.precioCompraProducto = precioCompraProducto;
        changeSupport.firePropertyChange("precioCompraProducto", oldPrecioCompraProducto, precioCompraProducto);
    }
/**
 * obtiene el precio de venta del producto
 * @return precioVentaProducto
 */
    public Double getPrecioVentaProducto() {
        return precioVentaProducto;
    }
/**
 * guarda l precio de venta del producto
 * @param precioVentaProducto 
 */
    public void setPrecioVentaProducto(Double precioVentaProducto) {
        Double oldPrecioVentaProducto = this.precioVentaProducto;
        this.precioVentaProducto = precioVentaProducto;
        changeSupport.firePropertyChange("precioVentaProducto", oldPrecioVentaProducto, precioVentaProducto);
    }
/**
 * obtiene el stock del producto
 * @return cntStockProducto
 */
    public Integer getCntStockProducto() {
        return cntStockProducto;
    }
/**
 * guarda la cantidad de stock del producto
 * @param cntStockProducto 
 */
    public void setCntStockProducto(Integer cntStockProducto) {
        Integer oldCntStockProducto = this.cntStockProducto;
        this.cntStockProducto = cntStockProducto;
        changeSupport.firePropertyChange("cntStockProducto", oldCntStockProducto, cntStockProducto);
    }
/**
 * obtiene la cantidad de stock critico (minimo) de un producto
 * @return cntStockCriticoProducto
 */
    public Integer getCntStockCriticoProducto() {
        return cntStockCriticoProducto;
    }
/**
 * guarda la cantidad de stock critico (minimo) de un producto
 * @param cntStockCriticoProducto 
 */
    public void setCntStockCriticoProducto(Integer cntStockCriticoProducto) {
        Integer oldCntStockCriticoProducto = this.cntStockCriticoProducto;
        this.cntStockCriticoProducto = cntStockCriticoProducto;
        changeSupport.firePropertyChange("cntStockCriticoProducto", oldCntStockCriticoProducto, cntStockCriticoProducto);
    }
    /**
     * obtiene el codunidad
     * @return codUnidad
     */

    public Integer getCodUnidad() {
        return codUnidad;
    }
/**
 * guarda el codunidad
 * @param codUnidad 
 */
    public void setCodUnidad(Integer codUnidad) {
        Integer oldCodUnidad = this.codUnidad;
        this.codUnidad = codUnidad;
        changeSupport.firePropertyChange("codUnidad", oldCodUnidad, codUnidad);
    }
/**
 * obtiene el codigo del proveedor
 * @return codProveedor
 */
    public Integer getCodProveedor() {
        return codProveedor;
    }
/**
 * guarda el codigo del proveedor
 * @param codProveedor 
 */
    public void setCodProveedor(Integer codProveedor) {
        Integer oldCodProveedor = this.codProveedor;
        this.codProveedor = codProveedor;
        changeSupport.firePropertyChange("codProveedor", oldCodProveedor, codProveedor);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codProducto != null ? codProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto)) {
            return false;
        }
        Producto other = (Producto) object;
        if ((this.codProducto == null && other.codProducto != null) || (this.codProducto != null && !this.codProducto.equals(other.codProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sistvent.busqueda.Producto[codProducto=" + codProducto + "]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

}
