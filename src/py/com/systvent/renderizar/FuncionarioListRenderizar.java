/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.systvent.renderizar;

import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import py.com.systvent.beans.Funcionario;

/**
 *
 * Clase para renderizar el nombre del funcionario
 * @author Maciel, Valdez, Duarte
 */
public class FuncionarioListRenderizar extends DefaultListCellRenderer {
    @Override
    public Component getListCellRendererComponent(
            JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        if(value instanceof Funcionario) { // Si el valor es una instancia de Funcionario
            Funcionario f = (Funcionario) value; // asigno el valor del funcionario
            setText(f.getNombreFuncionario()); // asignamos en nuestro caso el valor de la inscripción de la unidad
        }
        return this;
    }
}
